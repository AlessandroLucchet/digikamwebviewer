echo off
cls

echo Building backend...
cd %~dp0\Backend
dotnet publish --framework net7.0 --runtime win-x64 --configuration Release --no-self-contained --output bin\Release\net7.0\publish

echo Building frontend...
cd %~dp0\Frontend
call "quasar" build

echo Building setup rtf...
md %~dp0\Output
cd %~dp0\Output
pandoc -s ..\README.md -o README.rtf

echo Creating setup...
iscc %~dp0\Setup.iss

echo Clearing temp files...
del %~dp0\Output\README.rtf

pause