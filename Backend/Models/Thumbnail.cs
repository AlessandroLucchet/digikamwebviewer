﻿using System;

namespace digiKamWebViewer.Models
{
    /// <summary>
    /// Thumbnail of an image. It could be a picture, a video, a tag.
    /// Modification date is kept to update the thumbnail, if necessary.
    /// </summary>
    public class Thumbnail
    {
        public long imageid { get; set; }
        public long tagid { get; set; }
        public DateTime modificationDate { get; set; }
        public byte[] data { get; set; }
    }
}
