﻿namespace digiKamWebViewer.Models
{
    /// <summary>
    /// digiKam allows to set a picture for a tag. 
    /// This model contains the information about which picture is used as tag photo.
    /// This picture could be the entire picture, or a portion (rect) of it
    /// </summary>
    public class TagThumbnailProperty
    {
        public long imageid { get; set; }
        public string rect { get; set; }
    }
}
