﻿using System;
using System.Collections.Generic;

namespace digiKamWebViewer.Models
{
    /// <summary>
    /// Metadata of an image
    /// A image could be a picture a video ora an audio file
    /// </summary>
    public class Image
    {
        public long id { get; set; }
        public long album { get; set; }
        public string name { get; set; }
        public long fileSize { get; set; }
        public DateTime creationDate { get; set; }
        public DateTime digitalizationDate { get; set; }
        public long width { get; set; }
        public long height { get; set; }
        public String format { get; set; }
        public long orientation { get; set; }
        public double latitudeNumber { get; set; }
        public double longitudeNumber { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public string identifier { get; set; }
        public string specificPath { get; set; }
        public string relativePath { get; set; }
        public string mimeType { get { return mimeTypes.ContainsKey(format) ? mimeTypes[format] : ""; } }

        /// <summary>
        /// Get the file path
        /// </summary>
        /// <returns></returns>
        public string GetFilePath()
        {
            return VolumeMapping.GetInstance().getVolumePath(identifier) + "/" + specificPath + relativePath + "/" + name;
        }

        private static Dictionary<string, string> mimeTypes = new Dictionary<string, string>
        {
            {"JPG","image/jpeg"},
            {"GIF","image/gif"},
            {"PNG","image/png"},
            {"RAW-CR2","image/x-dcraw"},
            {"TIFF","image/tiff"},
            {"RAW-DNG","image/x-adobe-dng"},
            {"HEIF","image/heif"},
            {"AVI","video/x-msvideo"},
            {"WMV","video/x-ms-wmv"},
            {"MPEG","video/mpeg"},
            {"3GP","video/3gpp"},
            {"MP4","video/mp4"},
            {"MKV","video/x-matroska"},
            {"MOV","video/quicktime"},
            {"M4A","audio/m4a"},
            {"MP3","audio/mpeg"},
            {"OGG","audio/ogg"},
            {"WAV","audio/wav"},
            {"AAC","audio/aac"}
        };

        public bool IsPicture()
        {
            return format.Equals("JPG") || format.Equals("GIF") || format.Equals("PNG") || format.Equals("TIFF") || format.Equals("HEIF");
        }

        public bool IsVideo()
        {
            return format.Equals("AVI") || format.Equals("WMV") || format.Equals("MPEG") || format.Equals("3GP") || format.Equals("MP4") || format.Equals("MKV") || format.Equals("MOV");
        }

        public bool IsAudio()
        {
            return format.Equals("M4A") || format.Equals("MP3") || format.Equals("OGG") || format.Equals("WAV") || format.Equals("AAC");
        }
    }
}
