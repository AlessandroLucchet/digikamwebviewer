﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Management;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace digiKamWebViewer
{
    public class VolumeMapping
    {
		private static VolumeMapping instance;
		private Dictionary<String, String> mapping = null;

		public static VolumeMapping GetInstance()
		{
			if (instance == null)
				instance = new VolumeMapping();
			return instance;
		}

		/// <summary>
		/// Create a mapping between volume serial number and volume letter
		/// </summary>
		private VolumeMapping()
        {
			mapping = new Dictionary<string, string>();

			// If operating system is Windows
			if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				ManagementObjectSearcher ms = new ManagementObjectSearcher("Select * from Win32_LogicalDisk");
				foreach (ManagementObject mo in ms.Get())
				{
					if(mo["VolumeSerialNumber"]!=null)
						mapping.Add(mo["VolumeSerialNumber"].ToString().ToUpper(), mo["DeviceID"].ToString());
				}
			}
            else
            // If operating system is Linux
			if(System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                // Run lsblk to get mount points and related uuid
                Process process = new();
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = "-c \"lsblk -o UUID,MOUNTPOINT --json\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };

                // Get the output of lsblk
                process.StartInfo = startInfo;
                process.Start();
                string lsblkOutput = process.StandardOutput.ReadToEnd();
                process.WaitForExit();

                // Deserialize output and iterate through block devices
                BlockDeviceResponse response = JsonConvert.DeserializeObject<BlockDeviceResponse>(lsblkOutput);
                foreach (var blockDevice in response.BlockDevices)
                    if (blockDevice.UUID != null && blockDevice.MountPoint != null)
                        mapping.Add(blockDevice.UUID.ToUpper(), blockDevice.MountPoint);
            }
        }

		/// <summary>
		/// Get the drive letter of the passed volumeid
		/// </summary>
		/// <param name="uuid">The identifier stored in digiKam database</param>
		/// <returns></returns>
		public string getVolumePath(string uuid)
        {
			MatchCollection matches = Regex.Matches(uuid, "volumeid:\\?uuid=([a-fA-F0-9-]{8,})");
			foreach (Match match in matches)
				return mapping[match.Groups[1].Value.ToUpper()];
			return null;
        }
    }

    class BlockDevice
    {
        public string UUID { get; set; }
        public string MountPoint { get; set; }
    }

    class BlockDeviceResponse
    {
        public List<BlockDevice> BlockDevices { get; set; }
    }
}
