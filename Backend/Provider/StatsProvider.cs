﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using digiKamWebViewer.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace digiKamWebViewer.Repository
{
    public interface IStatsProvider
    {
        Task<IEnumerable<dynamic>> GetImagesStats();
        Task<long> GetThumbnailsCount();
        Task<long> GetTagsCount();
        Task<long> GetImageTagsCount();
        Task<long> GetAlbumsCount();
        Task<long> GetImagesCount();
    }

    public class StatsProvider : IStatsProvider
    {
        private readonly AppSettings appSettings;

        public StatsProvider(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        public async Task<IEnumerable<dynamic>> GetImagesStats()
        {
            using var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString());

            return await connection.QueryAsync<dynamic>("select format as Format, count(*) as Count, sum(fileSize) as Size from Images inner join ImageInformation on (ImageInformation.imageid = Images.id) group by format order by 2 desc");
        }

        public async Task<long> GetThumbnailsCount()
        {
            using var connection = new SqliteConnection(appSettings.GetThumbnailDatabaseConnectionString());

            var enumerator = (await connection.QueryAsync<long>("select count(*) as Count from Thumbnails")).GetEnumerator();
            if (enumerator.MoveNext())
                return enumerator.Current;
            else
                return 0;
        }

        public async Task<long> GetTagsCount()
        {
            using var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString());

            var enumerator = (await connection.QueryAsync<long>("select count(*) as Count from Tags")).GetEnumerator();
            if (enumerator.MoveNext())
                return enumerator.Current;
            else
                return 0;
        }

        public async Task<long> GetImageTagsCount()
        {
            using var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString());

            var enumerator = (await connection.QueryAsync<long>("select count(*) as Count from ImageTags")).GetEnumerator();
            if (enumerator.MoveNext())
                return enumerator.Current;
            else
                return 0;
        }

        public async Task<long> GetAlbumsCount()
        {
            using var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString());

            var enumerator = (await connection.QueryAsync<long>("select count(*) as Albums from Albums")).GetEnumerator();
            if (enumerator.MoveNext())
                return enumerator.Current;
            else
                return 0;
        }

        public async Task<long> GetImagesCount()
        {
            using var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString());

            var enumerator = (await connection.QueryAsync<long>("select count(*) as Images from Images")).GetEnumerator();
            if (enumerator.MoveNext())
                return enumerator.Current;
            else
                return 0;
        }
    }
}
