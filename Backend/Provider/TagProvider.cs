﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using digiKamWebViewer.Helpers;
using digiKamWebViewer.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace digiKamWebViewer.Repository
{
    public interface ITagProvider
    {
        Task<IEnumerable<Tag>> Get();
        Task<TagThumbnailProperty> GetThumbnailProperty(long tagid);
        Task<IEnumerable<Tag>> GetByIds(string ids);
        Task<IEnumerable<Tag>> GetByImage(long id);
        Task<IEnumerable<Tag>> GetAllTree();
    }

    public class TagProvider : ITagProvider
    {
        private readonly AppSettings appSettings;
        private static string fields = "id, pid, name, icon";

        public TagProvider(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Return all the tags
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Tag>> Get()
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                return await connection.QueryAsync<Tag>($"select {fields} from Tags where 1 not in (id, pid) order by pid, name");
            }
        }

        /// <summary>
        /// Return a tree of all tags
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Tag>> GetAllTree()
        {
            // Get flat List
            IEnumerable<Tag> flatList = await Get();

            // Setup a new list with root albums
            List<Tag> treeList = new List<Tag>();
            treeList.AddRange(flatList.Where(tag => tag.pid == 0));

            // Setup parent-child relations
            foreach (Tag parentEntry in flatList)
            {
                parentEntry.children = new List<Tag>();
                parentEntry.children.AddRange(flatList.Where(tag => tag.pid == parentEntry.id));
            }

            return treeList;
        }

        /// <summary>
        /// Return all the tags with specified ids
        /// </summary>
        /// <param name="ids">ids, delimited with comma</param>
        /// <returns></returns>
        public async Task<IEnumerable<Tag>> GetByIds(string ids)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                List<int> idsList = ids.Split(',').Select(System.Int32.Parse).ToList();
                return await connection.QueryAsync<Tag>($"select {fields} from Tags where 1 not in (id, pid) and id in @ids order by pid, name", new { ids = idsList });
            }
        }

        /// <summary>
        /// Return the image and the portion of the image used as thumbnail for this tag
        /// </summary>
        /// <param name="tagid"></param>
        /// <returns></returns>
        public async Task<TagThumbnailProperty> GetThumbnailProperty(long tagid)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                IEnumerator<TagThumbnailProperty> property = (await connection.QueryAsync<TagThumbnailProperty>("select ImageTagProperties.value as rect, Tags.icon as imageid from Tags left join ImageTagProperties on (Tags.icon = ImageTagProperties.imageid and Tags.id = ImageTagProperties.tagid) where 1 not in (Tags.id, Tags.pid) and Tags.id = @id", new { id = tagid })).GetEnumerator();
                if (property.MoveNext())
                    return property.Current;
                else
                    return null;
            }
        }

        /// <summary>
        /// Return the tags assigned to a specific picture
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Tag>> GetByImage(long id)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                return await connection.QueryAsync<Tag>($"select {fields} from Tags inner join ImageTags on (ImageTags.imageid = @imageid and ImageTags.tagid = Tags.id) where 1 not in (id, pid) order by pid, name", new { imageid = id });
            }
        }
    }
}
