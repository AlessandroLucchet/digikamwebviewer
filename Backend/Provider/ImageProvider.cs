﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using digiKamWebViewer.Helpers;
using digiKamWebViewer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace digiKamWebViewer.Repository
{
    public interface IImageProvider
    {
        Task<Image> GetById(long id);
        Task<IEnumerable<Image>> Get();
        Task<IEnumerable<Image>> GetByIds(string ids);
        Task<IEnumerable<Image>> GetByAlbum(long albumid);
        Task<IEnumerable<Image>> GetByTags(string tagids);
        Task<IEnumerable<Image>> GetByDate(int date);
        Task<IEnumerable<Image>> GetWithPosition();
    }

    public class ImageProvider : IImageProvider
    {
        private readonly AppSettings appSettings;
        private static string baseQuery = "select Images.id, Images.album, Images.name, AlbumRoots.identifier, AlbumRoots.specificPath, Albums.relativePath, Images.fileSize, ImagePositions.latitudeNumber, ImagePositions.longitudeNumber, ImageInformation.digitizationDate, ImageInformation.creationDate, Images.modificationDate, ImageInformation.width, ImageInformation.height, ImageInformation.[format], ImageInformation.orientation, ImageMetadata.make, ImageMetadata.model, VideoMetadata.duration " +
                "from Images " +
                "inner join Albums on (Images.album = Albums.id) " +
                "inner join AlbumRoots on (AlbumRoots.id = Albums.albumRoot) " +
                "inner join ImageInformation on (Images.id = ImageInformation.imageid) " +
                "left join ImagePositions on (Images.id = ImagePositions.imageid) " +
                "left join ImageMetadata on (Images.id = ImageMetadata.imageid) " +
                "left join VideoMetadata on (Images.id = VideoMetadata.imageid) ";
        private static string baseSort = "order by ImageInformation.digitizationDate, ImageInformation.creationDate, Images.modificationDate, Images.name";

        public ImageProvider(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Get metadata of all pictures
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Image>> Get()
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                return await connection.QueryAsync<Image>(baseQuery + " where Images.status = 1 " + baseSort);
            }
        }

        /// <summary>
        /// Get metadata of a specific picture
        /// </summary>
        /// <param name="id">The id of the picture</param>
        /// <returns></returns>
        public async Task<Image> GetById(long id)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                var enumerator = (await connection.QueryAsync<Image>(baseQuery + " where Images.status = 1 and Images.id = @imageid " + baseSort, new { imageid = id })).GetEnumerator();
                if (enumerator.MoveNext())
                    return enumerator.Current;
                else
                    return null;
            }
        }

        /// <summary>
        /// Get metadata of specific pictures
        /// </summary>
        /// <param name="tagids">A string containing the image ids separated by comma</param>
        /// <returns></returns>
        public async Task<IEnumerable<Image>> GetByIds(string ids)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                // Parse as ints
                List<int> idsSplitted = ids.Split(",").AsList().Select(int.Parse).ToList();
                // Run query
                return await connection.QueryAsync<Image>(baseQuery + " where Images.status = 1 and Images.id in @ids " + baseSort, new { ids = idsSplitted });
            }
        }

        /// <summary>
        /// Get images metadata of all pictures of an album
        /// </summary>
        /// <param name="albumid">The album id</param>
        /// <returns></returns>
        public async Task<IEnumerable<Image>> GetByAlbum(long albumid)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                return await connection.QueryAsync<Image>(baseQuery + " where Images.status = 1 and Images.album = @id " + baseSort, new { id = albumid });
            }
        }

        /// <summary>
        /// Get images metadata containing all specified tags
        /// </summary>
        /// <param name="tagids">A string containing the tag ids separated by comma</param>
        /// <returns></returns>
        public async Task<IEnumerable<Image>> GetByTags(string tagids)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                string tagJoins = "";
                string[] ids = tagids.Split(",");
                for (int x = 0; x < ids.Length; x++)
                    tagJoins += $"inner join ImageTags as ImageTags{x} on (ImageTags{x}.imageid = Images.id and ImageTags{x}.tagid={int.Parse(ids[x].Trim())}) ";
                return await connection.QueryAsync<Image>(baseQuery + tagJoins + " where Images.status = 1 " + baseSort);
            }
        }

        /// <summary>
        /// Get images metadata of a desired month or year
        /// </summary>
        /// <param name="date">A month or a year, represented YYYYmm or YYYY format</param>
        /// <returns></returns>
        public async Task<IEnumerable<Image>> GetByDate(int date)
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                string format = date < 10000 ? "%Y" : "%Y%m";
                return await connection.QueryAsync<Image>(baseQuery + $" where Images.status = 1 and strftime('{format}',IFNULL(ImageInformation.digitizationDate, IFNULL(ImageInformation.creationDate, Images.modificationDate))) = '{date}' " + baseSort);
            }
        }

        /// <summary>
        /// Get images metadata of pictures with geolocation
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Image>> GetWithPosition()
        {
            using (var connection = new SqliteConnection(appSettings.GetDigiKamDatabaseConnectionString()))
            {
                return await connection.QueryAsync<Image>(baseQuery + " where Images.status = 1 and ImagePositions.latitudeNumber is not null " + baseSort);
            }
        }
    }
}
