﻿using Microsoft.AspNetCore.Mvc;
using digiKamWebViewer.Helpers;
using digiKamWebViewer.Models;
using Microsoft.Extensions.Logging;

namespace digiKamWebViewer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IUserProvider _userService;
        private IPasswordHasher _passwordHasher;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IUserProvider userService, IPasswordHasher passwordHasher, ILogger<AuthController> logger)
        {
            _userService = userService;
            _passwordHasher = passwordHasher;
            _logger = logger;
        }

        [HttpPost]
        [Consumes("application/x-www-form-urlencoded")]
        public IActionResult Authenticate([FromForm] AuthenticateRequest model)
        {
            var response = _userService.Authenticate(model);

            if (response == null)
            {
                _logger.LogInformation($"Bad login attempt for '{model.Username}'");
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            else
            {
                _logger.LogInformation($"Login successful for '{model.Username}'");
                return Ok(response);
            }
        }

        /*[HttpPost("newuser")]
        [Consumes("application/x-www-form-urlencoded")]
        public IActionResult NewUser([FromForm] AuthenticateRequest model)
        {
            return Ok(_passwordHasher.Hash(model.Password));
        }*/
    }
}