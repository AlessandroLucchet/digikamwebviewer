﻿using digiKamWebViewer.Helpers;
using digiKamWebViewer.Models;
using digiKamWebViewer.Repository;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace digiKamWebViewer.Controllers
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [EnableCors]
    public class ImagesController : ControllerBase
    {
        private readonly IImageProvider imageProvider;
        private readonly IThumbnailProvider thumbnailProvider;
        private readonly IAlbumProvider albumProvider;
        private readonly ITagProvider tagProvider;
        private readonly AppSettings appSettings;

        // Don't allow  more  than 5 concurrent calls for the streaming
        private SemaphoreSlim StreamSemaphore = new SemaphoreSlim(5);

        public ImagesController(IThumbnailProvider thumbnailProvider, IImageProvider imageProvider, IAlbumProvider albumProvider, ITagProvider tagProvider, IOptions<AppSettings> appSettings)
        {
            this.thumbnailProvider = thumbnailProvider;
            this.imageProvider = imageProvider;
            this.albumProvider = albumProvider;
            this.tagProvider = tagProvider;
            this.appSettings = appSettings.Value;
        }

        public async Task<IEnumerable<Models.Image>> ListIds(string ids)
        {
            return await imageProvider.GetByIds(ids);
        }

        public async Task<IEnumerable<Models.Image>> ListAlbum(long id)
        {
            return await imageProvider.GetByAlbum(id);
        }

        public async Task<IEnumerable<Models.Image>> ListTags(string ids)
        {
            return await imageProvider.GetByTags(ids);
        }

        public async Task<IEnumerable<Models.Image>> ListDate(int date)
        {
            return await imageProvider.GetByDate(date);
        }

        public async Task<IEnumerable<Models.Image>> ListWithPosition()
        {
            return await imageProvider.GetWithPosition();
        }

        /// <summary>
        /// Download a zip with specific images
        /// </summary>
        /// <param name="ids">A string with image ids, comma delimited</param>
        /// <returns></returns>
        public async Task<IActionResult> DownloadIds(string ids)
        {
            IEnumerable<Image> images = await imageProvider.GetByIds(ids);
            return CreateZipFile(images, "images.zip");
        }

        /// <summary>
        /// Downlaod a zip with all the images of an album
        /// </summary>
        /// <param name="id">Album id</param>
        /// <returns></returns>
        public async Task<IActionResult> DownloadAlbum(long id)
        {
            IEnumerable<Image> images = await imageProvider.GetByAlbum(id);
            IEnumerable<Album> album = await albumProvider.GetById(id);
            if (album.Count() == 1 && images.Count() > 0)
                return CreateZipFile(images, album.First().name + ".zip");
            else
                return StatusCode(StatusCodes.Status404NotFound);
        }

        /// <summary>
        /// Download a zip with all the images with all the specified tags
        /// </summary>
        /// <param name="ids">A string with tag ids, comma delimited</param>
        /// <returns></returns>
        public async Task<IActionResult> DownloadTags(string ids)
        {
            IEnumerable<Image> images = await imageProvider.GetByTags(ids);
            IEnumerable<Tag> tags = await tagProvider.GetByIds(ids);

            if(images.Count() == 0)
                return StatusCode(StatusCodes.Status404NotFound);

            if (tags.Count() <= 4)
                return CreateZipFile(images, String.Join(" - ", tags.Select(tag => tag.name)) + ".zip");
            else
                return CreateZipFile(images, "tags.zip");
        }

        /// <summary>
        /// Download a zip with all the images of a specific date
        /// </summary>
        /// <param name="date">Integer of date. Could be in format: yyyy or yyyymm</param>
        /// <returns></returns>
        public async Task<IActionResult> DownloadDate(int date)
        {
            IEnumerable<Image> images = await imageProvider.GetByDate(date);

            if (images.Count() == 0)
                return StatusCode(StatusCodes.Status404NotFound);

            return CreateZipFile(images, date + ".zip");
        }

        /// <summary>
        /// Return the whole image stream (picture/video/audio)
        /// </summary>
        /// <param name="id">The image id</param>
        /// <returns></returns>
        public async Task<IActionResult> RawFile(long id)
        {
            // Get the path of the file
            digiKamWebViewer.Models.Image image = await imageProvider.GetById(id);
            if (image != null)
            {
                // Handle image format
                if (image.IsPicture())
                {
                    return PhysicalFile(image.GetFilePath(), image.mimeType, image.name);  //application/octet-stream
                }
                else if (image.IsVideo())
                {
                    var fileStream = new FileStream(image.GetFilePath(), FileMode.Open, FileAccess.Read, FileShare.Read);
                    return File(fileStream, image.mimeType, image.name, true);
                }
                else
                    throw new ArgumentException("Not handled file format");
            }
            return null;
        }

        /// <summary>
        /// Create a zip of the passed images
        /// </summary>
        /// <param name="images">The images to be zipped</param>
        /// <param name="zipName">The name of the zip file</param>
        /// <returns></returns>
        private FileStreamResult CreateZipFile(IEnumerable<Image> images, string zipName)
        {
            // Create a temp file
            string tempFilePath = Path.GetTempFileName();
            using (var stream = System.IO.File.OpenWrite(tempFilePath))
            {
                using (ZipArchive archive = new ZipArchive(stream, System.IO.Compression.ZipArchiveMode.Create))
                {
                    foreach (var item in images)
                        archive.CreateEntryFromFile(item.GetFilePath(), item.name, CompressionLevel.Fastest);
                }
            }

            // Return the temp file with DeleteOnClose flag
            var fs = new FileStream(tempFilePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose);
            return File(fileStream: fs, contentType: System.Net.Mime.MediaTypeNames.Application.Octet, fileDownloadName: zipName);
        }

        /// <summary>
        /// Generate a resized version of the specified image
        /// </summary>
        /// <param name="id">The image id</param>
        /// <param name="size">(optional) the desired image height size</param>
        /// <returns></returns>
        public async Task<IActionResult> Picture(long id, int size)
        {
            // Get the path of the file
            digiKamWebViewer.Models.Image image = await imageProvider.GetById(id);
            if (image != null)
            {
                MemoryStream picture = ThumbnailsFactory.CreateFromPicture(image.GetFilePath(), size != 0 ? size : appSettings.DefaultPictureMinimumSize);
                return File(picture.ToArray(), "image/jpeg");
            }
            return null;
        }

        /// <summary>
        /// Create a thumnbnail of the specified image
        /// </summary>
        /// <param name="id">The image id</param>
        /// <returns></returns>
        public async Task<IActionResult> Thumbnail(long id)
        {
            // Get metadatas of the image
            digiKamWebViewer.Models.Image image = await imageProvider.GetById(id);
            if (image != null)
            {
                // Handle image format
                if (image.IsPicture() || image.IsVideo() || image.IsAudio())
                {
                    // Get the thumbnail
                    Thumbnail tn = await thumbnailProvider.GetByImage(id);

                    // Return
                    if (tn != null)
                        return File(tn.data, "image/jpeg");
                    else
                        return UnprocessableEntity();
                }
                else
                    throw new ArgumentException("File unknown");
            }
            return null;
        }

        /// <summary>
        /// Create the index for stream the specified video
        /// </summary>
        /// <param name="id">The video id</param>
        /// <returns>An HLS m3u8 index file</returns>
        public async Task<IActionResult> StreamIndex(int id)
        {
            // Get the path of the file
            Image video = await imageProvider.GetById(id);
            if (video == null)
                return null;

            // Get the token
            string token = JwtMiddleware.GetToken(Request);

            // Create and return the index
            return Content(await StreamFactory.GetStreamIndex(video, appSettings.VideoSegmentDuration, token), "application/x-mpegURL");
        }


        /// <summary>
        /// Get a trascoded segment of the specified video
        /// </summary>
        /// <param name="id">The video id</param>
        /// <param name="segment">The segment number</param>
        /// <returns></returns>
        public async Task<FileStreamResult> Stream(int id, int segment)
        {
            FileStreamResult fileStreamResult = null;
            try
            {
                await StreamSemaphore.WaitAsync();

                // Get the path of the file
                Image video = await imageProvider.GetById(id);
                if (video == null)
                    return null;

                // Transcode and return the segment of the video
                fileStreamResult = File(await StreamFactory.GetStreamSegment(video, segment, appSettings.VideoSegmentDuration, appSettings.VideoCRF, appSettings.VideoMaxRate, appSettings.VideoScaleOption, appSettings.AudioBitRate), "video/MP2T", id + "_" + segment + ".ts");
            }
            finally
            {
                StreamSemaphore.Release();
            }

            return fileStreamResult;            
        }

    }
}