﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using digiKamWebViewer.Helpers;
using System;
using System.Linq;

namespace digiKamWebViewer.Database
{
    public interface IThumbnailDatabaseBootstrap
    {
        void Setup();
    }

    public class ThumbnailDatabase : IThumbnailDatabaseBootstrap
    {
        private readonly AppSettings appSettings;

        public ThumbnailDatabase(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        public void Setup()
        {
            var connection = new SqliteConnection(appSettings.GetThumbnailDatabaseConnectionString());

            int currentVersion = GetVersion(connection);
            int targetVersion = 1;

            while (currentVersion++ < targetVersion)
            { 
                switch(currentVersion)
                {
                    case 1:
                        connection.Execute("CREATE TABLE Thumbnails (imageid INTEGER, tagid INTEGER, modificationDate DATETIME, data BLOB, PRIMARY KEY(imageid,tagid))");
                        break;
                }
                SetVersion(connection,currentVersion);
            }
        }

        public int GetVersion(SqliteConnection connection)
        {
            int version = 0;
            connection.Open();
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "PRAGMA user_version;";
                cmd.ExecuteNonQuery();

                using (SqliteDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    version = Convert.ToInt32(reader["user_version"]);
                }
            }
            connection.Close();
            return version;
        }


        public void SetVersion(SqliteConnection connection, int version)
        {
            connection.Open();
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = $"PRAGMA user_version = {version};";
                cmd.ExecuteNonQuery();
            }
            connection.Close();
        }
    }
}
